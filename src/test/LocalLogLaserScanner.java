package test;

import java.util.ArrayList;
import java.util.List;

import framework.scanner.ILaserScanner;
import framework.scanner.MapPoint;

public class LocalLogLaserScanner implements ILaserScanner {

	LocalizerLogFile logFile;
	int significantMeasures = 30;
	public LocalLogLaserScanner(LocalizerLogFile logFile) {
		this.logFile = logFile;
	}

	@Override
	public List<MapPoint> singleScan() {
//		List<MapPoint> significantMeasuresList = new ArrayList<MapPoint>(significantMeasures);
//		double a = (double)logFile.getActualScanLog().getMapPoints().size() / significantMeasures;
//		for (int i = 0; i < significantMeasures; i++) {
//			significantMeasuresList.add(logFile.getActualScanLog().getMapPoints().get((int) (a * i)));
//		}
// 
//		return significantMeasuresList;
		return logFile.getActualScanLog().getMapPoints();
		
	}

}
