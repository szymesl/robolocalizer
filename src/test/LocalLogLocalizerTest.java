package test;

import java.io.File;

import framework.algorithm.Localizer;
import framework.algorithm.ParticleFilterModel;
import framework.algorithm.ParticleModelObserver;
import framework.robots.IRobotMovement;
import framework.robots.PandaRobotMovement;
import framework.scanner.ILaserScanner;
import gui.ParticleModelViewer;

public class LocalLogLocalizerTest {
	public static void main(String[] args) {
		try {
			LocalizerLogFile logFile = new LocalizerLogFile(new File("log 22.01/1970-01-01_01-28-007.txt"));
			IRobotMovement localLogRobotMovement = new LocalLogRobotMovement(logFile);
			
			ILaserScanner scanner = new LocalLogLaserScanner(logFile);
			File rosonFile = new File("makieta w robolabie obrys.dxf.roson");
//			File rosonFile = new File("2ndFloor-rooms.roson");
			Localizer localizer = new Localizer(rosonFile, localLogRobotMovement, scanner);
			ParticleFilterModel model = localizer.getModel();
			ParticleModelObserver observer = new ParticleModelViewer(model.getWalls());
			System.out.println("run");
			model.addObserver(observer);
			localizer.start();
//			Thread.sleep(10000);
//			localizer.stopLocalizing();
			localizer.join();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
