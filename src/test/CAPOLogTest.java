package test;

import java.io.File;

import pl.edu.agh.amber.common.AmberClient;
import pl.edu.agh.amber.roboclaw.RoboclawProxy;

import framework.algorithm.Localizer;
import framework.algorithm.ParticleFilterModel;
import framework.algorithm.ParticleModelObserver;
import framework.robots.IRobotMovement;
import framework.robots.PandaRobotMovement;
import framework.scanner.HokuyoLaserScanner;
import framework.scanner.ILaserScanner;
import gui.ParticleModelViewer;

public class CAPOLogTest {
	public static void main(String[] args) {
		try {
			LocalizerLogFile logFile = new LocalizerLogFile(new File(args[0]));
			IRobotMovement localLogRobotMovement = new LocalLogRobotMovement(logFile);
			AmberClient client = new AmberClient("127.0.0.1", 26233);
			RoboclawProxy roboclawProxy = new RoboclawProxy(client, 0);
			IRobotMovement robotMovement = new PandaRobotMovement(roboclawProxy);
			ILaserScanner scanner = new LocalLogLaserScanner(logFile);
			scanner = new HokuyoLaserScanner();
			File rosonFile = new File(args[1]);
//			File rosonFile = new File("2ndFloor-rooms.roson");
			Localizer localizer = new Localizer(rosonFile, robotMovement, scanner);
			System.out.println("run");
			localizer.start();
//			Thread.sleep(10000);
//			localizer.stopLocalizing();
			localizer.join();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
