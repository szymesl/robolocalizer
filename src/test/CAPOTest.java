package test;

import java.io.File;
import java.io.IOException;
import java.util.List;

import pl.edu.agh.amber.common.AmberClient;
import pl.edu.agh.amber.roboclaw.RoboclawProxy;
import framework.algorithm.Localizer;
import framework.algorithm.ParticleFilterModel;
import framework.algorithm.Position;
import framework.robots.IRobotMovement;
import framework.robots.PandaRobotMovement;
import framework.scanner.MapPoint;

public class CAPOTest {

	
	public static void main(String[] args) throws IOException, InterruptedException {
		System.out.println("CapoTest");
		AmberClient client = new AmberClient("127.0.0.1", 26233);
		RoboclawProxy roboclawProxy = new RoboclawProxy(client, 0);
		IRobotMovement robotMovement = new PandaRobotMovement(roboclawProxy);
		File rosonFile = new File(args[0]);
		Localizer localizer = new Localizer(rosonFile, robotMovement);
		localizer.start();
		Position actual, last;
		Thread.sleep(300);
		ParticleFilterModel model = localizer.getModel();
		actual = localizer.getActualLocation();
		List<MapPoint> mapPoints;
//		for (int i=0 ; i<50; i++) {
//			Thread.sleep(300);
//			last = actual;
//			actual = localizer.getActualLocation();
//			if (actual != null) System.out.println("dx: " + (actual.getX() - last.getX()) + " dy: " + (actual.getY() - last.getY()));
//			else System.out.println("Location null");
//			mapPoints = model.getActualScanLog();
//			if (mapPoints != null) System.out.println(mapPoints.get(mapPoints.size()/2).getDistance());
//			else System.out.println("Scan null");
//		}
	}
}
