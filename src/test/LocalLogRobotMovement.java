package test;

import java.io.IOException;

import pl.edu.agh.amber.roboclaw.MotorsCurrentSpeed;

import framework.algorithm.Particle;
import framework.robots.IRobotState;
import framework.robots.IRobotMovement;
import framework.robots.PandaRobotState;
import framework.robots.PandaRobotMovement;

public class LocalLogRobotMovement implements IRobotMovement {

	private LocalizerLogFile logFileKeeper;
	private PandaRobotMovement pandaRobotMovement;

	public LocalLogRobotMovement(LocalizerLogFile logFile) {
		this.logFileKeeper = logFile;
		try {
			this.pandaRobotMovement = new PandaRobotMovement(null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void translation(IRobotState previousMove, IRobotState currentMove, Particle particle) {
		pandaRobotMovement.translation(previousMove, currentMove, particle);
	}

	@Override
	public void rotation(IRobotState previousMove, IRobotState currentMove, Particle particle) {
		pandaRobotMovement.rotation(previousMove, currentMove, particle);
	}

	@Override
	public IRobotState getCurrentState() throws IOException, Exception {
		MotorsCurrentSpeed mcs = logFileKeeper.getCurrentMotorsSpeed();
		mcs.setAvailable();
		PandaRobotState pandaMove = new PandaRobotState(logFileKeeper.getActualTime(), mcs.getFrontLeftSpeed(), mcs.getFrontLeftSpeed(), mcs.getRearLeftSpeed(),
				mcs.getRearRightSpeed());
		return pandaMove;
	}

}
