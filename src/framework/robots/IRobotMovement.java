package framework.robots;

import java.io.IOException;

import framework.algorithm.Particle;

public interface IRobotMovement {
	
	public void translation(IRobotState previousMove, IRobotState currentMove, Particle particle);
	
	public void rotation(IRobotState previousMove, IRobotState currentMove, Particle particle);
	
	public IRobotState getCurrentState() throws IOException, Exception;

}
