package framework.robots;

import java.io.IOException;
import java.util.Random;

import pl.edu.agh.amber.roboclaw.MotorsCurrentSpeed;
import pl.edu.agh.amber.roboclaw.RoboclawProxy;
import framework.algorithm.Particle;

public class PandaRobotMovement implements IRobotMovement{

	private static Random random = new Random();
	private static final double rotationVarianceFactor = 0.7;
	private static final double rotationSpeedFactor = 0.07;
	private final static int robotWidth = 350; // TODO przechowywa� gdzie�
												// informacje o robocie i
												// stamtad to brac
	private static double atrs = 0.1; // TODO dobrac odpowiednie wartosci
	private static double adrft = 0.01; // TODO dobrac odpowiednie wartosci
	private static double speedFactor = 0.0001;
	
	private final RoboclawProxy roboclawProxy;

	public PandaRobotMovement(RoboclawProxy roboclawProxy) {
		this.roboclawProxy = roboclawProxy;
	}

	@Override
	public void rotation(IRobotState previousMove, IRobotState currentMove,
			Particle particle) {

		PandaRobotState pandaPrevMove = (PandaRobotState) previousMove;
		PandaRobotState pandaCurrMove = (PandaRobotState) currentMove;
		
		particle.setAngle(particle.getAngle() + PandaRobotMovement.getRotationWithNoise(pandaPrevMove, pandaCurrMove));
		
	}

	private static double getRotationWithNoise(PandaRobotState prev, PandaRobotState curr) {

		double lAvgWheelSpeed = (((prev.getlFrontWheelSpeed() + prev.getlRearWheelSpeed()) / 2) + ((curr.getlFrontWheelSpeed() + curr
				.getlRearWheelSpeed()) / 2)) / 2;
		double rAvgWheelSpeed = (((prev.getrFrontWheelSpeed() + prev.getrRearWheelSpeed()) / 2) + ((curr.getrFrontWheelSpeed() + curr
				.getrRearWheelSpeed()) / 2)) / 2;
		long deltaT = curr.getCurrentTime() - prev.getCurrentTime();
		double rotation = rotationSpeedFactor * (deltaT * (rAvgWheelSpeed - lAvgWheelSpeed)) / robotWidth;
		double noise = PandaRobotMovement.rotationNoise(rotation);
		return rotation + noise;
	}

	private static double rotationNoise(double rotationAngle) {

		// return rotationMaxNoise * Math.pow(Math.random(), 4); // more
		// efficient
		return random.nextGaussian() * rotationVarianceFactor * rotationAngle; // more accurate
	}

	@Override
	public void translation(IRobotState previousMove, IRobotState currentMove, Particle particle) {
		
		PandaRobotState pandaPrevMove = (PandaRobotState) previousMove;
		PandaRobotState pandaCurrMove = (PandaRobotState) currentMove;

		double deltaDist = deltaDist(pandaCurrMove.getCurrentTime() - pandaPrevMove.getCurrentTime(), pandaPrevMove, pandaCurrMove);
//		double mean = Mtrs * deltaDist;
//		double variance = atrs * deltaDist;
		Random gaus = new Random();
		// System.out.println("deltaDist: " + deltaDist + " : " +
		// curr.getlFrontWheelSpeed() + " : " + curr.getrFrontWheelSpeed());
		double edrft = gaus.nextGaussian() * adrft*deltaDist;
		double etrs = gaus.nextGaussian() * atrs * deltaDist;
		particle.setAngle(particle.getAngle() + edrft);
		particle.setX(particle.getX() + ((deltaDist + etrs) * Math.cos(Math.toRadians(particle.getAngle()))));
		particle.setY(particle.getY() + ((deltaDist + etrs) * Math.sin(Math.toRadians(particle.getAngle()))));
	}

	private static double deltaDist(long time, PandaRobotState prev, PandaRobotState curr) {

		double prevAvg = (prev.getlFrontWheelSpeed() + prev.getrFrontWheelSpeed() + prev.getlRearWheelSpeed() + prev.getrRearWheelSpeed()) / 4;
		double currAvg = (curr.getlFrontWheelSpeed() + curr.getrFrontWheelSpeed() + curr.getlRearWheelSpeed() + curr.getrRearWheelSpeed()) / 4;
		
		double toReturn = speedFactor * time * ((prevAvg + currAvg) / 2); 
		
		return toReturn;
	}
	
	@Override
	public IRobotState getCurrentState() throws IOException, Exception {
		MotorsCurrentSpeed motorsCurrentSpeed = this.roboclawProxy.getCurrentMotorsSpeed();
		
		PandaRobotState pandaMove = new PandaRobotState(System.currentTimeMillis(), motorsCurrentSpeed.getFrontLeftSpeed(), motorsCurrentSpeed.getFrontRightSpeed(), motorsCurrentSpeed.getRearLeftSpeed(), motorsCurrentSpeed.getRearRightSpeed());
		
		return pandaMove;
	}

}
