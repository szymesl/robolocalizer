package framework.algorithm;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import pl.edu.agh.amber.roboclaw.RoboclawProxy;
import framework.robots.IRobotMovement;
import framework.robots.IRobotState;
import framework.scanner.HokuyoLaserScanner;
import framework.scanner.ILaserScanner;
import framework.scanner.MapPoint;

public class Localizer extends Thread {

	private int iii;
	private ParticleFilterModel model;


	private static int startingParticleAmmount = 100;
	private static int standardParticleAmmount = 50;

	// Weight function parameters
	private static int significantMeasures = 15;
	private static final double maxLaserRange = 560;
	private static final double standardDeviation = 50.0;

	// Particles modification parameters
	// 0.0 - 1.0; percentage of all particles that should be randomly placed to
	// prevent unpredictable changes of robot position
	private static final double randomParticlesPercentage = 0.2;
	// 0.0 - 1.0; percentage of all particles that should be left after we find
	// robot position with high probability
	private static final double goodPostionParticlesPercentage = 0.5;
	private final double resampleThreshold = 0.25; // TODO read from config file
	private final double resampleOverheadThreshold = 0.6; // TODO read from
															// config file
	private final double randomThreshold = 0.01;
	private final double randomOverheadThreshold = 0.9;

	private final IRobotMovement robotMovement;
	private Boolean isRunning = true;
	private ILaserScanner laserScanner;

	public synchronized Position getActualLocation() {
		return model.getLastPosition();
	}

	private synchronized void setActualLocation(Position position) {
		model.setLastPosition(position);
	}

	public Localizer(File rosonFile, IRobotMovement robotMovement) throws IOException {
		this.laserScanner = new HokuyoLaserScanner();
		this.model = new ParticleFilterModel(rosonFile);
		this.robotMovement = robotMovement;
	}

	public Localizer(File rosonFile, IRobotMovement robotMovement, ILaserScanner laserScanner) throws IOException {
		this.model = new ParticleFilterModel(rosonFile);
		this.robotMovement = robotMovement;
		this.laserScanner = laserScanner;
	}

	public void run() {
		try {

			Particle np = new Particle(260, 50, 85, 1.0 / startingParticleAmmount);
			model.addParticle(np);
			for (int i = 0; i < startingParticleAmmount; i++) {
				model.addParticle(model.randParticle(1.0 / startingParticleAmmount));
			}
			IRobotState currentRobotMovement = robotMovement.getCurrentState();
			IRobotState previousRobotMovement;

			double effSampleSize = 0.0;
			long lastTime = System.currentTimeMillis();
			long currTime = lastTime;
			long startWeight = 0;
			long stopWeight = 0;
			
			while (isRunning()) {
				
				model.setActualScanLog(laserScanner.singleScan());
				previousRobotMovement = currentRobotMovement;
				currentRobotMovement = robotMovement.getCurrentState();

				lastTime = currTime;
				currTime = System.currentTimeMillis();
				System.out.println("Localize loop time (" +  + model.getParticles().size() + " particles, " + significantMeasures + " rays): " + (currTime - lastTime));
				System.out.println("Weight time: " + (stopWeight - startWeight));
				
				currTime = System.currentTimeMillis();

				double maxWeight = 0;

				List<MapPoint> significantMeasuresList = new ArrayList<MapPoint>(significantMeasures);
				if (significantMeasures < model.getActualScanLog().size()) {
					double a = (double) model.getActualScanLog().size() / significantMeasures;
					for (int i = 0; i < significantMeasures; i++) {
						significantMeasuresList.add(model.getActualScanLog().get((int) (a * i)));
					}
				} else
					significantMeasuresList = model.getActualScanLog();

				iii = 0;
				startWeight = System.currentTimeMillis();
				for (Particle particle : model.getParticles()) { // UPDATE
					double weight = calculateParticleAdjustment(particle, significantMeasuresList);
					maxWeight = Math.max(maxWeight, weight);
					particle.setWeight(particle.getWeight() + weight / significantMeasures);

				}
				stopWeight = System.currentTimeMillis();
				System.out.println("Intersection calculation count: " + iii);

				// NORMALIZE
	//			model.normalizeParticles();

				// resample(maxWeight);
				model.notifyParticleModification();

				List<Particle> particles = model.getParticles();

				// System.out.println(effectiveSampleSize() + " < " +
				// (model.getParticles().size() * resampleThreshold));
				effSampleSize = effectiveSampleSize();
				
// ACHTUNG WARNIGN UWAGA REMOVE THISS:		//TODO		
				effSampleSize = 100000;
				
				if (effSampleSize < (model.getParticles().size() * resampleThreshold)) {
					// RESAMPLE
					// System.out.println("RESAMPLE");
					double[] resParticles = new double[model.getParticles().size() + 1];
					Random gaus = new Random();
					for (int i = 0; i < resParticles.length; i++) {
						resParticles[i] = Math.abs(gaus.nextGaussian()) * model.getMaxWeight();
					}
					Arrays.sort(resParticles);
					double cumSum = 0;
					double resSum = resParticles[0];

					int j = 0;
					int index = (int) (Math.random() * (model.getParticles().size() - 1));
					int last = (index - 1 + model.getParticles().size()) % model.getParticles().size();
					System.out.println("last: " + last);
					Particle particle = null, lastp = model.getParticles().get(last);

					for (int i = 0; i < model.getParticles().size(); i++) {
						last = (index + i) % model.getParticles().size();
						particle = model.getParticles().get(last);

						cumSum += particle.getWeight();
						if (cumSum < resSum) {
							particle.setAngle(lastp.getAngle());
							particle.setWeight(lastp.getWeight());
							particle.setLocation(lastp.getX(), lastp.getY());
						} else {
							// while( resSum < cumSum ) resSum +=
							// resParticles[j++];
							resSum += resParticles[j++];
							if (lastp.getWeight() < model.getParticles().get(last).getWeight()) {
								lastp = model.getParticles().get(last);
							}
						}
					}
				}
				
				effSampleSize = effectiveSampleSize();
	// ACHTUNG WARNIGN UWAGA REMOVE THISS:		//TODO		
	effSampleSize = 100000;
				if (effSampleSize < (model.getParticles().size() * randomThreshold)
						|| effSampleSize > (model.getParticles().size() * randomOverheadThreshold)) {
					// RANDOM PLACEMENT OF PARTICLES
					if (effSampleSize > (model.getParticles().size() * randomOverheadThreshold))
						System.out.println("RANDOM UP");
					if (effSampleSize < (model.getParticles().size() * randomThreshold))
						System.out.println("RANDOM DOWN");
					Particle particle = null, party = null;
					Collections.sort(particles, new ParticleWeightComparator());
					for (int i = 0; i < (model.getParticles().size() * randomParticlesPercentage); i++) {
						// particleCpy =
						model.getParticles().get(model.getParticles().size() - 1 - i);
						// new position could be outside the map - more
						// efficient
						// particleCpy = model.randParticle(1.0 /
						// model.getParticles().size());

						particle = model.getParticles().get(model.getParticles().size() - 1 - i);
						party = model.randParticle(1.0 / model.getParticles().size()).copy();
						particle.setAngle(party.getAngle());
						particle.setLocation(party.getX(), party.getY());
						particle.setWeight(1.0 / model.getParticles().size());
					}
				} else {
					// RANDOM PLACEMENT OF PARTICLES OUTSIDE THE MAP
					Particle particle = null, party = null;
					for (int i = 0; i < model.getParticles().size(); i++) {
						particle = model.getParticles().get(i);
						if (!model.pointInside(particle)) {
							party = model.randParticle(1.0 / model.getParticles().size()).copy();
							particle.setAngle(party.getAngle());
							particle.setLocation(party.getX(), party.getY());
							particle.setWeight(1.0 / model.getParticles().size());
						}
					}
				}
				for (Particle particle : model.getParticles()) { // PREDICTION
					// ROTATION
					
	// ACHTUNG WARNIGN UWAGA REMOVE THISS:		//TODO		
	//				robotMovement.rotation(previousRobotMovement, currentRobotMovement, particle);
					// particle.setAngle(particle.getAngle() +
					// PandaRobotMovement.getRotationWithNoise(previousRobotMovement,
					// currentRobotMovement));
					// TRANSPORT
	// ACHTUNG WARNIGN UWAGA REMOVE THISS:		//TODO		
	//				robotMovement.translation(previousRobotMovement, currentRobotMovement, particle);
					// TODO
					// PandaRobotMovement.translation(previousRobotMovement,
					// currentRobotMovement, particle);
				}

				// System.out.println(previousRobotMovement);
				// System.out.println(currentRobotMovement);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// Blad wczytania roboclawa, wsadzic do loggera
			e.printStackTrace();
		}

	}

	//
	// private void resample(double maxWeight) {
	// int count = model.getParticles().size();
	// List<Particle> particles = model.getParticles();
	// List<Particle> newParticles = new ArrayList<Particle>(count);
	//
	// double[] t = new double[model.getParticles().size() +1];
	// double[] q = new double[model.getParticles().size()];
	// q[0]=particles.get(0).getWeight();
	// t[0] = Math.random();
	// for (int i=1; i<count; i++) {
	// t[i] = Math.random();
	// q[i] = q[i-1] + particles.get(i).getWeight();
	// }
	// t[count] = Math.random();
	// Arrays.sort(t);
	// int jump = 1;
	// int i=0;
	// int j=i;
	// while (i < count-jump ) {
	// if (t[i] < q[j]) {
	// newParticles.add(particles.get(j).copy());
	// i += jump;
	// }
	// else
	// j += jump;
	// }
	// for (int it=0; it<newParticles.size(); it++) {
	// if (newParticles.get(it).getWeight() < (0.1/newParticles.size())) {
	// newParticles.set(it, model.randParticle(1/newParticles.size()));
	// }
	// }
	// model.setParticles(newParticles);
	// }

	private synchronized boolean isRunning() {
		return isRunning;
	}

	private double effectiveSampleSize() {
		double sum = 0;
		int particleammount = model.getParticles().size();
		for (Particle particle : model.getParticles()) {
			sum += Math.pow(((particleammount * particle.getWeight()) - 1), 2);
		}
		return particleammount / (1 + (sum / particleammount));
	}

	private double calculateParticleAdjustment(Particle p, List<MapPoint> measures) {

		double adjustment = 0.0;
		List<Line2D> relevantWalls = model.getRelevantWalls(p);
		if (relevantWalls.size() == 0) { // it means that point is outside
			return 0;
		}
		double x1, x2, x3, x4, y1, y2, y3, y4;
		x1 = p.getX();
		y1 = p.getY();
		for (MapPoint measure : measures) {

			double measureDistance = 0.1 * measure.getDistance();
			double minDistance = Double.MAX_VALUE;
			x2 = p.getX() + maxLaserRange * Math.cos(Math.toRadians(p.getAngle() + measure.getAngle()));
			y2 = p.getY() + maxLaserRange * Math.sin(Math.toRadians(p.getAngle() + measure.getAngle()));
			for (Line2D wall : relevantWalls) {
				x3 = wall.getX1();
				y3 = wall.getY1();
				x4 = wall.getX2();
				y4 = wall.getY2();

				iii++; //TODO
				double x = ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4));
				double y = ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4));
				boolean intersectFirstLine;
				boolean intersectSecondLine; 
				if ((x1 - x2)*(x1 - x2) > (y1 - y2)*(y1 - y2))
					intersectFirstLine =  (x < x1 && x > x2) || (x > x1 && x < x2);
				else intersectFirstLine = (y < y1 && y > y2) || (y > y1 && y < y2);
				if ((x3 - x4)*(x3 - x4) > (y3 - y4)*(y3 - y4)) 
					intersectSecondLine = (x < x3 && x > x4) || (x > x3 && x < x4);
				else
					intersectSecondLine = (y < y3 && y > y4) || (y > y3 && y < y4);
				if ( intersectFirstLine && intersectSecondLine) {
					minDistance = Math.min(minDistance, Math.sqrt((x - x1) * (x - x1) + (y - y1) * (y - y1)));
				}

			}

			double pd = minDistance - measureDistance;
			double SDFactor = 0.1;
			double standardDeviation = SDFactor * measureDistance;
			double probability;
			if (measure.getDistance() < 50) { // if distance -> 0 probability
												// blow into Universe
				standardDeviation = Double.MAX_VALUE;
				probability = 0;
			} else {
				if (measureDistance == 0.0)
					measureDistance = maxLaserRange;
				probability = Math.exp(-(pd * pd) / (2 * standardDeviation * standardDeviation));
			}
			adjustment += probability;
		}
		return adjustment;
	}

	public synchronized void stopLocalizing() {
		System.out.println("stop");
		this.isRunning = false;
	}

	public ParticleFilterModel getModel() {
		return this.model;
	}

}
