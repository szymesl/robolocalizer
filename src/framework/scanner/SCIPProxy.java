package framework.scanner;
import java.util.List;



public class SCIPProxy {
	
	private SCIP scip;

	public SCIPProxy(SCIP scip){
		this.scip = scip;
	}
	
	public synchronized List<MapPoint> singleScan(){
		return scip.singleScan();
	}
}
