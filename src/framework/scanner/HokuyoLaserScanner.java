package framework.scanner;

import java.io.IOException;
import java.util.List;

import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;

public class HokuyoLaserScanner implements ILaserScanner {

	private SCIP scip;
	public HokuyoLaserScanner () {
		Serial.getAvailablePorts();
		System.out.print("Type port name: ");
		// Scanner sc = new Scanner(System.in);
		// String port = sc.nextLine();
		String port = "/dev/ttyS8";
		// sc.close();
		scip = null;
		SerialPort serialPort;
		try {
			serialPort = SerialPortHelper.getHokuyoSerialPort(port);
			scip = new SCIP(serialPort.getInputStream(), serialPort.getOutputStream());
		} catch (PortInUseException | IOException | NoSuchPortException | UnsupportedCommOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		scip.laserOn();
	}
	
	@Override
	public List<MapPoint> singleScan() {
		
		return scip.singleScan();
	}

}
